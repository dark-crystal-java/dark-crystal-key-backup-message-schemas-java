package org.magmacollective.gradle;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.logging.Logger;

public class GenerateSchemasPlugin implements Plugin<Project> {

    @Override
    public void apply(final Project project) {
        Logger logger = project.getLogger();
        logger.info("applying generate schemas plugin");

        GenerateSchemasExtension extension = project.getExtensions().create(
                "generateSchemas",
                GenerateSchemasExtension.class);

        GenerateSchemasTask task = project.getTasks().create(
                "generateSchemas",
                GenerateSchemasTask.class);
        task.setDescription("Generate .xsd files from the .jsons files");
        task.setConfiguration(extension);

        project.getTasks().findByName("compileJava").dependsOn(task);
    }

}
