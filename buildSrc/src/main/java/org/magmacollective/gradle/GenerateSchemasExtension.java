package org.magmacollective.gradle;

import java.util.List;

public class GenerateSchemasExtension {

    private List<String> input;

    public List<String> getInput() {
        return input;
    }

    public void setInput(List<String> input) {
        this.input = input;
    }

}
