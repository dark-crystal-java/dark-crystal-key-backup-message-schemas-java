package org.magmacollective.gradle;

import com.ethlo.jsons2xsd.Config;
import com.ethlo.jsons2xsd.Jsons2Xsd;
import org.gradle.api.InvalidUserDataException;
import org.gradle.api.Project;
import org.gradle.api.internal.ConventionTask;
import org.gradle.api.tasks.TaskAction;
import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class GenerateSchemasTask extends ConventionTask {

    private GenerateSchemasExtension extension;

    public GenerateSchemasTask() {
        setGroup("build");
    }

    public void setConfiguration(GenerateSchemasExtension extension) {
        this.extension = extension;
    }

    @TaskAction
    protected void copyFiles() throws IOException, TransformerException {
        Project project = getProject();

        if (extension.getInput() == null) {
            throw new InvalidUserDataException("You need to specify the input files via 'input'");
        }

        System.out.println("Generating schemas from files:");

        List<String> fileNames = extension.getInput();

        Path projectDir = project.getProjectDir().toPath();
        Path buildDir = project.getBuildDir().toPath();

        for (String fileName : fileNames) {
            Path input = projectDir.resolve(fileName);
            String outputFileName = fileName.replaceFirst(".json", ".xsd");
            Path output = buildDir.resolve(outputFileName);

            if (isOutdated(input, output)) {
                continue;
            }

            System.out.println(String.format("generating '%s' from '%s'",
                    projectDir.relativize(output), projectDir.relativize(input)));

            Files.createDirectories(output.getParent());
            convertSchema(input, output);
        }
    }

    private boolean isOutdated(Path input, Path output) throws IOException {
        if (!Files.exists(output)) {
            return false;
        }
        return Files.getLastModifiedTime(input).toInstant().isBefore(Files.getLastModifiedTime(output).toInstant());
    }

    private void convertSchema(Path input, Path output) throws IOException, TransformerException {
        try (final Reader r = Files.newBufferedReader(input)) {
            final Config cfg = new Config.Builder()
                    .targetNamespace("http://example.com/myschema.xsd")
                    .name("array")
                    .build();
            final Document doc = Jsons2Xsd.convert(r, cfg);
            write(doc, output);
        }
    }

    private void write(Document doc, Path output) throws IOException, TransformerException {
        OutputStream os = Files.newOutputStream(output);
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        tr.transform(new DOMSource(doc),
                new StreamResult(os));
    }

}
