package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.magmacollective.darkcrystal.keybackup.messageschemas.ShardV1.Shard;

import java.security.SecureRandom;

class TestShard {
  @Test void testShard() throws Exception {
    final long timeStamp = System.currentTimeMillis();
    final ByteString recipient = ByteString.copyFrom("recipients public key".getBytes());
    final ByteString shardData = ByteString.copyFrom("shard data".getBytes());
    final ByteString rootId = ByteString.copyFrom("root id".getBytes());
    Shard shard = Shard.newBuilder()
            .setVersion("1.0.0")
            .setType("dark-crystal/shard")
            .setTimestamp(timeStamp)
            .setRecipient(recipient)
            .setShard(shardData)
            .setRoot(rootId)
            .build();
    final byte[] shardByteArray = shard.toByteArray();
    Shard shardParsed = Shard.parseFrom(shardByteArray);
    assertTrue(shardParsed.isInitialized(), "All required fields set");
    assertTrue(shardParsed.getVersion().equals("1.0.0"), "Correct version");
    assertTrue(shardParsed.getType().equals("dark-crystal/shard"), "Correct type");
    assertTrue(shardParsed.getTimestamp() == timeStamp, "Correct timestamp");
    assertTrue(shardParsed.getRecipient().equals(recipient), "Correct recipient");
    assertTrue(shardParsed.getShard().equals(shardData), "Correct recipient");
    assertTrue(shardParsed.getRoot().equals(rootId), "Correct recipient");
  }
}