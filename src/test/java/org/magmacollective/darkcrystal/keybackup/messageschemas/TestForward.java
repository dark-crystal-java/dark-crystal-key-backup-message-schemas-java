package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.google.protobuf.ByteString;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.magmacollective.darkcrystal.keybackup.messageschemas.ForwardV1.Forward;

class TestForward {
  @Test void testForward() throws Exception {
    final long timeStamp = System.currentTimeMillis();
    final ByteString recipient = ByteString.copyFrom("recipients public key".getBytes());
    final ByteString shardData = ByteString.copyFrom("shard data".getBytes());
    final ByteString rootId = ByteString.copyFrom("root id".getBytes());
    final ByteString branch = ByteString.copyFrom("branch reference".getBytes());
    Forward forward = Forward.newBuilder()
            .setVersion("1.0.0")
            .setType("dark-crystal/forward")
            .setTimestamp(timeStamp)
            .setRecipient(recipient)
            .setShard(shardData)
            .setRoot(rootId)
            .setBranch(branch)
            .build();
    final byte[] forwardByteArray = forward.toByteArray();
    Forward forwardParsed = Forward.parseFrom(forwardByteArray);
    assertTrue(forwardParsed.isInitialized(), "All required fields set");
    assertTrue(forwardParsed.getVersion().equals("1.0.0"), "Correct version");
    assertTrue(forwardParsed.getType().equals("dark-crystal/forward"), "Correct type");
    assertTrue(forwardParsed.getTimestamp() == timeStamp, "Correct timestamp");
    assertTrue(forwardParsed.getRecipient().equals(recipient), "Correct recipient");
    assertTrue(forwardParsed.getShard().equals(shardData), "Correct shard data");
    assertTrue(forwardParsed.getRoot().equals(rootId), "Correct root reference");
    assertTrue(forwardParsed.getBranch().equals(branch), "Correct branch reference");
  }
}