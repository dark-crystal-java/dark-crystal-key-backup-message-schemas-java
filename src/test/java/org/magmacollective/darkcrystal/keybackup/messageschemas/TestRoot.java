package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.magmacollective.darkcrystal.keybackup.messageschemas.RootV1.Root;

import java.security.SecureRandom;

class TestRoot {
    @Test void testRoot() throws Exception {
        SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
        final byte[] tagByteArray = new byte[32];
        secureRandom.nextBytes(tagByteArray);
        final ByteString tag = ByteString.copyFrom(tagByteArray);
        final long timeStamp = System.currentTimeMillis();
        Root root = Root.newBuilder()
                .setVersion("1.0.0")
                .setType("dark-crystal/root")
                .setTimestamp(timeStamp)
                .setQuorum(3)
                .setShards(5)
                .setName("directions to treasure")
                .setTool("dsprenkles/sss")
                .setTag(tag)
                .build();
        final byte[] rootByteArray = root.toByteArray();
        Root rootParsed = Root.parseFrom(rootByteArray);
        assertTrue(rootParsed.isInitialized(), "All required fields set");
        assertTrue(rootParsed.getVersion().equals("1.0.0"), "Correct version");
        assertTrue(rootParsed.getType().equals("dark-crystal/root"), "Correct type");
        assertTrue(rootParsed.getTimestamp() == timeStamp, "Correct timestamp");
        assertTrue(rootParsed.getQuorum() == 3, "Correct quorum");
        assertTrue(rootParsed.getShards() == 5, "Correct number of shards");
        assertTrue(rootParsed.getName().equals("directions to treasure"), "Correct name");
        assertTrue(rootParsed.getTool().equals("dsprenkles/sss"), "Correct tool");
        assertTrue(rootParsed.getTag().equals(tag), "Correct tag");
     }
}
