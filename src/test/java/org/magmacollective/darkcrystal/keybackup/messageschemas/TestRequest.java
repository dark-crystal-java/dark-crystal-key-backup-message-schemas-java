package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.magmacollective.darkcrystal.keybackup.messageschemas.RequestV1.Request;

class TestRequest {
  @Test void testRequest() throws Exception {
    final long timeStamp = System.currentTimeMillis();
    final ByteString recipient = ByteString.copyFrom("recipients public key".getBytes());
    final ByteString rootId = ByteString.copyFrom("root id".getBytes());
    final ByteString ephemeralKey = ByteString.copyFrom("ephemeral public key".getBytes());
    Request request = Request.newBuilder()
            .setVersion("1.0.0")
            .setType("dark-crystal/request")
            .setTimestamp(timeStamp)
            .setRecipient(recipient)
            .setRoot(rootId)
            .setEphemeralPublicKey(ephemeralKey)
            .build();
    final byte[] requestByteArray = request.toByteArray();
    Request requestParsed = Request.parseFrom(requestByteArray);
    assertTrue(requestParsed.isInitialized(), "All required fields set");
    assertTrue(requestParsed.getVersion().equals("1.0.0"), "Correct version");
    assertTrue(requestParsed.getType().equals("dark-crystal/request"), "Correct type");
    assertTrue(requestParsed.getTimestamp() == timeStamp, "Correct timestamp");
    assertTrue(requestParsed.getRecipient().equals(recipient), "Correct recipient");
    assertTrue(requestParsed.getRoot().equals(rootId), "Correct root id");
    assertTrue(requestParsed.getEphemeralPublicKey().equals(ephemeralKey),
            "Correct ephemeral public key");
  }

  @Test void testRequestWithSecretOwnerId() throws Exception {
    final long timeStamp = System.currentTimeMillis();
    final ByteString recipient = ByteString.copyFrom("recipients public key".getBytes());
    final ByteString secretOwnerId = ByteString.copyFrom("secretOwnerId".getBytes());
    final ByteString ephemeralKey = ByteString.copyFrom("ephemeral public key".getBytes());
    Request request = Request.newBuilder()
            .setVersion("1.0.0")
            .setType("dark-crystal/request")
            .setTimestamp(timeStamp)
            .setRecipient(recipient)
            .setSecretOwnerId(secretOwnerId)
            .setEphemeralPublicKey(ephemeralKey)
            .build();
    final byte[] requestByteArray = request.toByteArray();
    Request requestParsed = Request.parseFrom(requestByteArray);
    assertTrue(requestParsed.isInitialized(), "All required fields set");
    assertTrue(requestParsed.getVersion().equals("1.0.0"), "Correct version");
    assertTrue(requestParsed.getType().equals("dark-crystal/request"), "Correct type");
    assertTrue(requestParsed.getTimestamp() == timeStamp, "Correct timestamp");
    assertTrue(requestParsed.getRecipient().equals(recipient), "Correct recipient");
    assertTrue(requestParsed.getSecretOwnerId().equals(secretOwnerId), "Correct root id");
    assertTrue(requestParsed.getEphemeralPublicKey().equals(ephemeralKey),
            "Correct ephemeral public key");
  }
}