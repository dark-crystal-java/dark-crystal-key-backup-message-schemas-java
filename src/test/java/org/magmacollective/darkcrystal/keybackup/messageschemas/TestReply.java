package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.magmacollective.darkcrystal.keybackup.messageschemas.ReplyV1.Reply;

class TestReply {
  @Test void testReply() throws Exception {
    final long timeStamp = System.currentTimeMillis();
    final ByteString recipient = ByteString.copyFrom("recipients public key".getBytes());
    final ByteString shardData = ByteString.copyFrom("shard data".getBytes());
    final ByteString rootId = ByteString.copyFrom("root id".getBytes());
    final ByteString branch = ByteString.copyFrom("branch reference".getBytes());
    Reply reply = Reply.newBuilder()
            .setVersion("1.0.0")
            .setType("dark-crystal/reply")
            .setTimestamp(timeStamp)
            .setRecipient(recipient)
            .setShard(shardData)
            .setRoot(rootId)
            .setBranch(branch)
            .build();
    final byte[] replyByteArray = reply.toByteArray();
    Reply replyParsed = Reply.parseFrom(replyByteArray);

    assertTrue(replyParsed.isInitialized(), "All required fields set");
    assertTrue(replyParsed.getVersion().equals("1.0.0"), "Correct version");
    assertTrue(replyParsed.getType().equals("dark-crystal/reply"), "Correct type");
    assertTrue(replyParsed.getTimestamp() == timeStamp, "Correct timestamp");
    assertTrue(replyParsed.getRecipient().equals(recipient), "Correct recipient");
    assertTrue(replyParsed.getShard().equals(shardData), "Correct shard data");
    assertTrue(replyParsed.getRoot().equals(rootId), "Correct root reference");
    assertTrue(replyParsed.getBranch().equals(branch), "Correct branch reference");
  }
}