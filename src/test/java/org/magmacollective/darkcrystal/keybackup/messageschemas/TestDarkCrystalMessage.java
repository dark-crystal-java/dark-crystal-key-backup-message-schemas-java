package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.google.protobuf.ByteString;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestDarkCrystalMessage {
  @Test void testDarkCrystalMessage() throws Exception {

    final long timeStamp = System.currentTimeMillis();
    final ByteString recipient = ByteString.copyFrom("recipients public key".getBytes());
    final ByteString shardData = ByteString.copyFrom("shard data".getBytes());
    final ByteString rootId = ByteString.copyFrom("root id".getBytes());
    final ByteString branch = ByteString.copyFrom("branch reference".getBytes());
    ReplyV1.Reply reply = ReplyV1.Reply.newBuilder()
            .setVersion("1.0.0")
            .setType("dark-crystal/reply")
            .setTimestamp(timeStamp)
            .setRecipient(recipient)
            .setShard(shardData)
            .setRoot(rootId)
            .setBranch(branch)
            .build();
    DarkCrystalMessage.DarkCrystal message = DarkCrystalMessage.DarkCrystal.newBuilder().setReply(reply).build();
    ReplyV1.Reply replyOutput = message.getReply();
    assertTrue(replyOutput.isInitialized(), "All required fields set");
    assertEquals("1.0.0", replyOutput.getVersion(), "Correct version");
    assertTrue(replyOutput.getType().equals("dark-crystal/reply"), "Correct type");
    assertTrue(replyOutput.getTimestamp() == timeStamp, "Correct timestamp");
    assertTrue(replyOutput.getRecipient().equals(recipient), "Correct recipient");
    assertTrue(replyOutput.getShard().equals(shardData), "Correct shard data");
    assertTrue(replyOutput.getRoot().equals(rootId), "Correct root reference");
    assertTrue(replyOutput.getBranch().equals(branch), "Correct branch reference");
  }
}