package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.ValidationMessage;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.magmacollective.darkcrystal.keybackup.messageschemas.JsonValidator.getJsonNodeFromStringContent;
import static org.magmacollective.darkcrystal.keybackup.messageschemas.JsonValidator.getJsonSchemaFromStringContent;

public class TestJsonValidator {

//  @Test
//  @DisplayName("Test JSON schema validation")
//  public void testSchema() throws Exception {
//    JsonSchema schema = getJsonSchemaFromStringContent("{\"enum\":[1, 2, 3, 4],\"enumErrorCode\":\"Not in the list\"}");
//    JsonNode node = getJsonNodeFromStringContent("7");
//    Set<ValidationMessage> errors = schema.validate(node);
//    assertEquals(errors.size(), 1);
//  }

  @Test
  @DisplayName("Test JSON schema - root")
  public void testSchemaRoot() throws Exception {
    JsonValidator jsonValidator = new JsonValidator();
    JsonNode node = JsonValidator.getJsonNodeFromFile("rootFixtureV1.json");
    Set<ValidationMessage> errors = jsonValidator.isRoot(node);
    assertEquals(errors.size(), 0);
  }

  @Test
  @DisplayName("Test JSON schema - shard")
  public void testSchemaShard() throws Exception {
    JsonValidator jsonValidator = new JsonValidator();
    JsonNode node = JsonValidator.getJsonNodeFromFile("shardFixtureV1.json");
    Set<ValidationMessage> errors = jsonValidator.isShard(node);
    assertEquals(errors.size(), 0);
  }

  @Test
  @DisplayName("Test JSON schema - request")
  public void testSchemaRequest() throws Exception {
    JsonValidator jsonValidator = new JsonValidator();
    JsonNode node = JsonValidator.getJsonNodeFromFile("requestFixtureV1.json");
    Set<ValidationMessage> errors = jsonValidator.isRequest(node);
    assertEquals(errors.size(), 0);
  }

  @Test
  @DisplayName("Test JSON schema - request with secret owner ID")
  public void testSchemaRequestWithSecretOwner() throws Exception {
    JsonValidator jsonValidator = new JsonValidator();
    JsonNode node = JsonValidator.getJsonNodeFromFile("requestWithSecretOwnerFixtureV1.json");
    Set<ValidationMessage> errors = jsonValidator.isRequest(node);
    assertEquals(errors.size(), 0);
  }

  @Test
  @DisplayName("Test JSON schema - reply")
  public void testSchemaReply() throws Exception {
    JsonValidator jsonValidator = new JsonValidator();
    JsonNode node = JsonValidator.getJsonNodeFromFile("replyFixtureV1.json");
    Set<ValidationMessage> errors = jsonValidator.isReply(node);
    assertEquals(errors.size(), 0);
  }

  @Test
  @DisplayName("Test JSON schema - forward")
  public void testSchemaForward() throws Exception {
    JsonValidator jsonValidator = new JsonValidator();
    JsonNode node = JsonValidator.getJsonNodeFromFile("forwardFixtureV1.json");
    Set<ValidationMessage> errors = jsonValidator.isForward(node);
    assertEquals(errors.size(), 0);
  }
}
