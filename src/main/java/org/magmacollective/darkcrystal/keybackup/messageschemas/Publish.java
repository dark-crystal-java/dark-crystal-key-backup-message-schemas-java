package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.google.protobuf.ByteString;

import org.magmacollective.darkcrystal.keybackup.crypto.EdDSA;
import org.magmacollective.darkcrystal.keybackup.crypto.KeyBackupCrypto;

/**
 * Encode and Decode protobuf messages for the five message types
 */
public class Publish {
  /**
   * Current version number
   */
  public static final String VERSION = "1.0.0";
  /**
   * Current library used
   */
  public static final String TOOL = "dsprenkles/sss";
  /**
   * Root ID length in bytes
   */
  public static final int ROOT_ID_LENGTH = KeyBackupCrypto.SYMMETRIC_KEY_BYTES;
  /**
   * Public signing key length in bytes
   */
  public static final int PUBLIC_KEY_LENGTH = EdDSA.PUBLIC_KEY_LENGTH;
  /**
   * Public encryption key length in bytes
   */
  public static final int CURVE25519_PUBLIC_KEY_BYTES = KeyBackupCrypto.CURVE25519_PUBLIC_KEY_BYTES;
  /**
   * Hash length in bytes
   */
  public static final int BLAKE2B_BYTES = KeyBackupCrypto.BLAKE2B_BYTES;
  //public final int SIGNED_SHARD_MIN_LENGTH = KeyBackupCrypto.SIGNED_SHARD_MIN_LENGTH;
  /**
   * Signed Shard length in bytes
   */
  public static final int SIGNED_SHARD_MIN_LENGTH = 33 + EdDSA.SIGNATURE_LENGTH + KeyBackupCrypto.MAC_BYTES + KeyBackupCrypto.NONCE_BYTES;

  private void assertThat(boolean condition, String message) throws Exception {
    if (!condition) throw new Exception(message);
  }

  /**
   * Build Dark Crystal Key Backup messages using Protobuf
   */
  public static class BuildMessage {

    /**
     * Build a root message
     *
     * @param quorum the threshold
     * @param shards the number of shards
     * @param name   a name for the secret (not visible to custodians)
     * @param tag    a random tag to make the hash unique
     * @return a root message as a byte array
     */
    public byte[] buildRoot(int quorum, int shards, String name, byte[] tag) {
      RootV1.Root root = RootV1.Root.newBuilder()
          .setVersion(VERSION)
          .setType("dark-crystal/root")
          .setTimestamp(System.currentTimeMillis())
          .setQuorum(quorum)
          .setShards(shards)
          .setName(name)
          .setTool(TOOL)
          .setTag(ByteString.copyFrom(tag))
          .build();

      DarkCrystalMessage.DarkCrystal message = DarkCrystalMessage.DarkCrystal.newBuilder().setRoot(root).build();
      return message.toByteArray();
      // TODO could also validate by calling decodeRoot before returning
    }

    /**
     * Build a shard message
     *
     * @param rootId
     * @param recipient
     * @param shardData
     * @return a shard message as a byte array
     */
    public byte[] buildShard(byte[] rootId, byte[] recipient, byte[] shardData) {
      ShardV1.Shard shard = ShardV1.Shard.newBuilder()
          .setVersion(VERSION)
          .setType("dark-crystal/shard")
          .setTimestamp(System.currentTimeMillis())
          .setRecipient(ByteString.copyFrom(recipient))
          .setShard(ByteString.copyFrom(shardData))
          .setRoot(ByteString.copyFrom(rootId))
          .build();

      return DarkCrystalMessage.DarkCrystal
        .newBuilder()
        .setShard(shard)
        .build()
        .toByteArray();
    }

    /**
     * Build a request message
     *
     * @param recipient
     * @param rootId
     * @param ephemeralPublicKey
     * @return
     */
    public byte[] buildRequest(byte[] recipient, byte[] rootId, byte[] ephemeralPublicKey) {
      RequestV1.Request request = RequestV1.Request.newBuilder()
          .setVersion(VERSION)
          .setType("dark-crystal/request")
          .setTimestamp(System.currentTimeMillis())
          .setRecipient(ByteString.copyFrom(recipient))
          .setRoot(ByteString.copyFrom(rootId))
          .setEphemeralPublicKey(ByteString.copyFrom(ephemeralPublicKey))
          .build();
      return request.toByteArray();
    }

    /**
     * Build a request message, without giving an ephemeral public key
     *
     * @param recipient
     * @param rootId
     * @return a request message as a byte array
     */
    public byte[] buildRequest(byte[] recipient, byte[] rootId) {
      RequestV1.Request request = RequestV1.Request.newBuilder()
          .setVersion(VERSION)
          .setType("dark-crystal/request")
          .setTimestamp(System.currentTimeMillis())
          .setRecipient(ByteString.copyFrom(recipient))
          .setRoot(ByteString.copyFrom(rootId))
          .build();
      return request.toByteArray();
    }

    /**
     * Build a request message, giving the public key of the secret ower rather than a root Id
     * This is for requesting a forward - when we do not know the root ID.
     *
     * @param recipient the public key of the recipient of the request
     * @param secretOwnerId the public key of the original secret owner
     * @return a request message as a byte array
     */
    public byte[] buildRequestBySecretOwner(byte[] recipient, byte[] secretOwnerId) {
      RequestV1.Request request = RequestV1.Request.newBuilder()
              .setVersion(VERSION)
              .setType("dark-crystal/request")
              .setTimestamp(System.currentTimeMillis())
              .setRecipient(ByteString.copyFrom(recipient))
              .setSecretOwnerId(ByteString.copyFrom(secretOwnerId))
              .build();
      return request.toByteArray();
    }

    /**
     * Build a reply message
     *
     * @param recipient
     * @param rootId
     * @param shardData
     * @param branch
     * @return a reply message as a byte array
     */
    public byte[] buildReply(byte[] recipient, byte[] rootId, byte[] shardData, byte[] branch) {
      return ReplyV1.Reply.newBuilder()
          .setVersion(VERSION)
          .setType("dark-crystal/reply")
          .setTimestamp(System.currentTimeMillis())
          .setRecipient(ByteString.copyFrom(recipient))
          .setRoot(ByteString.copyFrom(rootId))
          .setShard(ByteString.copyFrom(shardData))
          .setBranch(ByteString.copyFrom(branch))
          .build()
          .toByteArray();
    }

    /**
     * Build a forward message
     *
     * @param recipient the public key of the recipient
     * @param rootId the identifier of the secret
     * @param shardData the shard itself
     * @param branch a reference to the associated 'request' message (optional)
     * @return a forward message as a byte array
     */
    public byte[] buildForward(byte[] recipient, byte[] rootId, byte[] shardData, byte[] branch) {
      return ForwardV1.Forward.newBuilder()
          .setVersion(VERSION)
          .setType("dark-crystal/forward")
          .setTimestamp(System.currentTimeMillis())
          .setRecipient(ByteString.copyFrom(recipient))
          .setRoot(ByteString.copyFrom(rootId))
          .setShard(ByteString.copyFrom(shardData))
          .setBranch(ByteString.copyFrom(branch))
          .build()
          .toByteArray();
    }

    /**
     * Build a forward message, without giving a branch reference
     *
     * @param recipient the public key of the recipient
     * @param rootId the identifier of the secret
     * @param shardData the shard itself
     * @return a forward message as a byte array
     */
    public byte[] buildForward(byte[] recipient, byte[] rootId, byte[] shardData) {
      return ForwardV1.Forward.newBuilder()
              .setVersion(VERSION)
              .setType("dark-crystal/forward")
              .setTimestamp(System.currentTimeMillis())
              .setRecipient(ByteString.copyFrom(recipient))
              .setRoot(ByteString.copyFrom(rootId))
              .setShard(ByteString.copyFrom(shardData))
              .build()
              .toByteArray();
    }
  }

  public class DecodeMessage {

    /**
     * Decode a root message
     */
    public RootV1.Root decodeRoot(byte[] rootMessage) throws Exception {
      RootV1.Root rootParsed = RootV1.Root.parseFrom(rootMessage);
      assertThat(rootParsed.isInitialized(), "Root message badly formed");
      assertThat(rootParsed.getVersion() == VERSION, "Mismatched version, should be " + VERSION);
      assertThat(rootParsed.getType() == "dark-crystal/root", "Not a valid root message");
      assertThat(rootParsed.getQuorum() < rootParsed.getShards(), "Quorum too high");
      assertThat(rootParsed.getQuorum() > 1, "Quorum must be at least 2");
      assertThat(rootParsed.getShards() > 1, "Number of shards must be at least 2");
      assertThat(rootParsed.getShards() < 256, "Number of shards cannot be greater than 255");
      assertThat(rootParsed.getTool() == TOOL, "Tool must be " + TOOL);
      return rootParsed;
    }

    /**
     * Validate a given shard message
     *
     * @param shardMessage the message to be parsed
     * @return a validated, parsed shard message
     * @throws Exception If the message had any invalid properties
     */
    public ShardV1.Shard decodeShard(byte[] shardMessage) throws Exception {
      ShardV1.Shard shardParsed = ShardV1.Shard.parseFrom(shardMessage);
      assertThat(shardParsed.isInitialized(), "Root message badly formed");
      assertThat(shardParsed.getVersion() == VERSION, "Mismatched version, should be " + VERSION);
      assertThat(shardParsed.getType() == "dark-crystal/shard", "Not a valid shard message");
      assertThat(shardParsed.getRoot().size() == ROOT_ID_LENGTH, "Root ID is incorrect length");
      assertThat(shardParsed.getShard().size() > SIGNED_SHARD_MIN_LENGTH, "Shard data must be at least " + SIGNED_SHARD_MIN_LENGTH);
      assertThat(shardParsed.getRecipient().size() ==  PUBLIC_KEY_LENGTH, "Invalid public key");
      return shardParsed;
    }

    /**
     * Validate a given request message
     *
     * @param requestMessage
     * @return
     * @throws Exception
     */
    public RequestV1.Request decodeRequest(byte[] requestMessage) throws Exception {
      RequestV1.Request requestParsed = RequestV1.Request.parseFrom(requestMessage);
      assertThat(requestParsed.isInitialized(), "Request message badly formed");
      assertThat(requestParsed.getVersion() == VERSION, "Mismatched version, should be " + VERSION);
      assertThat(requestParsed.getType() == "dark-crystal/request", "Not a valid request message");
      assertThat(requestParsed.getRecipient().size() ==  PUBLIC_KEY_LENGTH, "Invalid public key");
      assertThat(requestParsed.hasRoot() || requestParsed.hasSecretOwnerId(), "Either a rood id, or a secret owner id must be given");
      if (requestParsed.hasRoot()) {
        assertThat(requestParsed.getRoot().size() == ROOT_ID_LENGTH, "Root ID is incorrect length");
      } else {
        assertThat(requestParsed.getSecretOwnerId().size() == PUBLIC_KEY_LENGTH, "Secret owner ID is incorrect length");
      }
        if (requestParsed.hasEphemeralPublicKey()) {
        assertThat(requestParsed.getEphemeralPublicKey().size() == CURVE25519_PUBLIC_KEY_BYTES, "Invalid ephemeral key");
      }
      return requestParsed;
    }

    /**
     * Validate a given reply message
     *
     * @param replyMessage
     * @return a reply message object
     * @throws Exception if the given reply message could not be validated
     */
    public ReplyV1.Reply decodeReply(byte[] replyMessage) throws Exception {
      ReplyV1.Reply replyParsed = ReplyV1.Reply.parseFrom(replyMessage);
      assertThat(replyParsed.isInitialized(), "Reply message badly formed");
      assertThat(replyParsed.getVersion() == VERSION, "Mismatched version, should be " + VERSION);
      assertThat(replyParsed.getType() == "dark-crystal/reply", "Not a valid request message");
      assertThat(replyParsed.getRoot().size() == ROOT_ID_LENGTH, "Root ID is incorrect length");
      assertThat(replyParsed.getShard().size() > SIGNED_SHARD_MIN_LENGTH, "Shard data must be at least " + SIGNED_SHARD_MIN_LENGTH);
      assertThat(replyParsed.getRecipient().size() ==  PUBLIC_KEY_LENGTH, "Invalid public key");
      assertThat(replyParsed.getBranch().size() == BLAKE2B_BYTES, "Invalid branch");
      return replyParsed;
    }

    /**
     * Validate a given forward message
     *
     * @param forwardMessage
     * @return
     * @throws Exception
     */
    public ForwardV1.Forward decodeForward(byte[] forwardMessage) throws Exception {
      ForwardV1.Forward forwardParsed = ForwardV1.Forward.parseFrom(forwardMessage);
      assertThat(forwardParsed.isInitialized(), "Forward message badly formed");
      assertThat(forwardParsed.getVersion() == VERSION, "Mismatched version, should be " + VERSION);
      assertThat(forwardParsed.getType() == "dark-crystal/forward", "Not a valid request message");
      assertThat(forwardParsed.getRoot().size() == ROOT_ID_LENGTH, "Root ID is incorrect length");
      assertThat(forwardParsed.getShard().size() > SIGNED_SHARD_MIN_LENGTH, "Shard data must be at least " + SIGNED_SHARD_MIN_LENGTH);
      assertThat(forwardParsed.getRecipient().size() ==  PUBLIC_KEY_LENGTH, "Invalid public key");
      if (forwardParsed.hasBranch())
        assertThat(forwardParsed.getBranch().size() == BLAKE2B_BYTES, "Invalid branch");
      return forwardParsed;
    }
  }
}

