package org.magmacollective.darkcrystal.keybackup.messageschemas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.ValidationMessage;

import java.io.InputStream;
import java.util.Set;

public class JsonValidator {
  private JsonSchema rootV1;
  private JsonSchema shardV1;
  private JsonSchema requestV1;
  private JsonSchema replyV1;
  private JsonSchema forwardV1;

  public JsonValidator() throws Exception {
    this.rootV1 = getJsonSchemaFromFile("rootV1.json");
    this.shardV1 = getJsonSchemaFromFile("shardV1.json");
    this.requestV1 = getJsonSchemaFromFile("requestV1.json");
    this.replyV1 = getJsonSchemaFromFile("replyV1.json");
    this.forwardV1 = getJsonSchemaFromFile("forwardV1.json");
  }

  /**
   * Tests if a given message is a well-formed JSON 'root' message
   * @param node a JsonNode message
   * @return a set of error ValidationMessage - if it has a size of 0, it is a well formed message
   */
  public Set<ValidationMessage> isRoot(JsonNode node) {
    return this.rootV1.validate(node);
  }

  /**
   * Tests if a given message is a well-formed JSON 'shard' message
   * @param node a JsonNode message
   * @return a set of error ValidationMessage - if it has a size of 0, it is a well formed message
   */
  public Set<ValidationMessage> isShard(JsonNode node) {
    return shardV1.validate(node);
  }

  /**
   * Tests if a given message is a well-formed JSON 'request' message
   * @param node a JsonNode message
   * @return a set of error ValidationMessage - if it has a size of 0, it is a well formed message
   */
  public Set<ValidationMessage> isRequest(JsonNode node) {
    return requestV1.validate(node);
  }

  /**
   * Tests if a given message is a well-formed JSON 'reply' message
   * @param node a JsonNode message
   * @return a set of error ValidationMessage - if it has a size of 0, it is a well formed message
   */
  public Set<ValidationMessage> isReply(JsonNode node) {
    return replyV1.validate(node);
  }

  /**
   * Tests if a given message is a well-formed JSON 'forward' message
   * @param node a JsonNode message
   * @return a set of error ValidationMessage - if it has a size of 0, it is a well formed message
   */
  public Set<ValidationMessage> isForward(JsonNode node) {
    return forwardV1.validate(node);
  }

  /**
   * Takes a JSON string and returns a JsonNode object
   * @param content a string which should be well-formed JSON
   * @return a JsonNode object
   * @throws Exception if there is a parsing error
   */
  public static JsonNode getJsonNodeFromStringContent(String content) throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode node = mapper.readTree(content);
    return node;
  }

  /**
   * Takes a JSON string and gives a JsonSchema object
   * @param schemaContent a json-schema.org schema
   * @return a JsonSchema object
   * @throws Exception if there is a problem parsing
   */
  public static JsonSchema getJsonSchemaFromStringContent(String schemaContent) throws Exception {
    JsonSchemaFactory factory = JsonSchemaFactory.getInstance();
    JsonSchema schema = factory.getSchema(schemaContent);
    return schema;
  }

  /**
   * get a JsonNode from a file on the classpath
   * @param name a filename
   * @return a JsonNode
   * @throws Exception if there is a parsing error
   */
  public static JsonNode getJsonNodeFromFile(String name) throws Exception {
    InputStream input = Thread.currentThread().getContextClassLoader()
            .getResourceAsStream(name);
    ObjectMapper mapper = new ObjectMapper();
    JsonNode node = mapper.readTree(input);
    return node;
  }

  /**
   * Get a JsonSchema from a file on the classpath
   * @param name a filename
   * @return a JsonSchema object
   * @throws Exception if there is an error parsing
   */
  public static JsonSchema getJsonSchemaFromFile(String name) throws Exception {
    JsonSchemaFactory factory = JsonSchemaFactory.getInstance();
    InputStream input = Thread.currentThread().getContextClassLoader()
            .getResourceAsStream(name);
    JsonSchema schema = factory.getSchema(input);
    return schema;
  }
}
