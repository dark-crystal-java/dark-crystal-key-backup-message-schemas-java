#!/bin/bash
protoc --java_out=../java/. rootV1.proto
protoc --java_out=../java/. shardV1.proto
protoc --java_out=../java/. requestV1.proto
protoc --java_out=../java/. replyV1.proto
protoc --java_out=../java/. forwardV1.proto
protoc --java_out=../java/. darkCrystalMessage.proto
