# Dark Crystal Key Backup Message Schemas 


The Dark Crystal key backup protocol has five message types which are described below. 

Template schemas are provided in [protocol buffers](https://developers.google.com/protocol-buffers), [json-schema](https://json-schema.org), and [XML-Schema-Definition (XSD)](https://www.w3.org/TR/xmlschema11-2/). Generally, we recommend using protocol buffers as they are compact and fast to parse. But we have included JSON and XML schemas as they are popular encoding schemes, and it might make sense to use them if your project already uses them for other kinds of messages.

These can be seen as templates, as you may want to modify them based on the individual needs of your project. But they can also be simply used as they are.

The XSD schemas are generated from the JSON schemas using a conversion tool. A gradle task `generateSchemas` is provided which can be used to build them. 

## JSON Validators

The JSON validators provide methods to check if a given JSON object is structured correctly as one of the five possible message types. See [API documentation](https://dark-crystal-java.gitlab.io/dark-crystal-key-backup-java-docs/dark-crystal-key-backup-message-schemas/org/magmacollective/darkcrystal/keybackup/messageschemas/JsonValidator.html) for description of these methods.
 .

## Schemas

All messages contain:
- A `type` field describing which kind of message it is. All type names also include the words 'dark-crystal' to avoid confusion with other message types your application might use.
- A `version` number of the schema to allow backward compatibility when updating the protocol. This is represented as a string, using the [semantic versioning convention](https://semver.org/), and is currently `"1.0.0"`.
- A `timestamp` giving a record of what happened when. This is represented using [unix time](https://en.wikipedia.org/wiki/Unix_time).

Depending on whether it is implicit to the transport protocol used, it may also be appropriate to include an `author` field, containing the public key of the author of the message.

### `root`

This message will be built exactly once for each shared secret, and will contain a name for the secret.  It is only for the purpose of the secret owner having a record of the backup of the secret. Custodians must not be able to read it.

Generally, the `root` message is only stored locally on disk by the secret owner.  However, if using a transport protocol based on append-only logs, it may make sense to encrypt it to the secret owner, and publish it to the log, so that it can be retrieved following account recovery. 

As well as serving as a record for the secret owner, it is also referred to by the other messages, so we can be sure which shards are associated with which secret. Its reference is known as the "root ID", and is the hash of the message content. In some cases it might make sense to rather use a reference to where the root message is stored (for example in an append-only log, the feed-id and sequence number of the encrypted message). This way, we have a way of establishing that a collection of shard messages belong to the same secret, without revealing any additional metadata.

A 32 byte random 'tag' is included in the message to ensure that given the hash of the message, the other metadata cannot be 'guessed'.

#### JSON Example:

```json
{
  "type": "dark-crystal/root",
  "version": "1.0.0",
  "name": "directions to treasure",
  "quorum": 2,
  "shards": 5,
  "tool": "dsprenkles/sss",
  "tag": "032bccebb4f9f8c2b5d3e884186b700758906a1c1a56f4dd0b9ce7a540b765f1",
  "timestamp": 1580300916260 
}
```

### `shard`

This message will be published once for each shard of the secret. It will contain a reference to the `root` message for that secret, as well as the shard itself. The shard will be encrypted with the public key of the recipient of the shard. This will be a private message with exactly two recipients, one of which will be the author of the message. Note that there are two levels of encryption here, which means that the shard itself is not exposed to the author but the rest of the message is. This allows the author to keep track of who shards have been sent to as well as to verify shard integrity when receiving the decrypted shard later.

#### JSON Example: 

```json
{
  "type": "dark-crystal/shard",
  "version": "1.0.0",
  "root": <reference to root message>,
  "shard": <shard data>,
  "recipient": <public key of recipient>, 
  "timestamp": 1580300916260 
}
```
### `request`

This message is sent by the secret owner to their shard-holders, upon the need to recover the secret. It will contain a reference to the `root` message for that secret. Optionally, when being used in the context of an append-only log, including an ephemeral public key in the request, the reply is encrypted with the ephemeral key. 

#### JSON Example: 

```json
{ 
  "type": "dark-crystal/request",
  "version": "1.0.0",
  "recipient": <public key of recipient>, 
  "root": <reference to root message>,
  "ephPublicKey": <one-time public key>,
  "timestamp": 1580300916260 
}
```

### `reply`

A reply message is sent by a shard-holder on the request of the secret owner when the secret-owner's request is sent from the original account, defined by it being sent with the secret-owner's original key, being the same as that with which the shard was originally created.

#### JSON Example: 

```json
{
  "type": "dark-crystal/reply",
  "version": "1.0.0",
  "recipient": <public key of recipient>,
  "root": <reference to root message>,
  "branch": <reference to request message>,
  "shard": <encrypted shard data>,
  "timestamp": 1580300916260 
}
```
### `forward`

This message will be sent in order to send a shard to a different recipient key than that which authored the shard message. It will be a private message which exactly two recipients, one of whom will be the author of the message.  It will also contain:

- a reference to the associated `root` message
- the version number of the shard it contains
- an unencrypted shard

### JSON Example:

```json
{
  "type": "dark-crystal/forward",
  "version": "1.0.0",
  "root": <reference to root message>,
  "shard": <encrypted shard data>
  "recipient": <public key of recipient>,
  "timestamp": 1580300916260
}
```

